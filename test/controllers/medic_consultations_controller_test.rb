require 'test_helper'

class MedicConsultationsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @medic_consultation = medic_consultations(:one)
  end

  test "should get index" do
    get medic_consultations_url
    assert_response :success
  end

  test "should get new" do
    get new_medic_consultation_url
    assert_response :success
  end

  test "should create medic_consultation" do
    assert_difference('MedicConsultation.count') do
      post medic_consultations_url, params: { medic_consultation: { diagnosis: @medic_consultation.diagnosis, dni: @medic_consultation.dni, medicine: @medic_consultation.medicine, user_id: @medic_consultation.user_id } }
    end

    assert_redirected_to medic_consultation_url(MedicConsultation.last)
  end

  test "should show medic_consultation" do
    get medic_consultation_url(@medic_consultation)
    assert_response :success
  end

  test "should get edit" do
    get edit_medic_consultation_url(@medic_consultation)
    assert_response :success
  end

  test "should update medic_consultation" do
    patch medic_consultation_url(@medic_consultation), params: { medic_consultation: { diagnosis: @medic_consultation.diagnosis, dni: @medic_consultation.dni, medicine: @medic_consultation.medicine, user_id: @medic_consultation.user_id } }
    assert_redirected_to medic_consultation_url(@medic_consultation)
  end

  test "should destroy medic_consultation" do
    assert_difference('MedicConsultation.count', -1) do
      delete medic_consultation_url(@medic_consultation)
    end

    assert_redirected_to medic_consultations_url
  end
end
