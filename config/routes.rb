Rails.application.routes.draw do
  get 'visitors/index'

  resources :consultations
  resources :medic_consultations
  devise_for :users
  resources :patients
  resources :medicines
  resources :diagnoses
  resources :events
  authenticated :user do
 	  root 'visitors#index'
  end

  unauthenticated :user do
   	root 'welcome#index'
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
