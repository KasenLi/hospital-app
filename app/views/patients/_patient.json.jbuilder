json.extract! patient, :id, :name, :last_name, :sex, :birthday, :age, :created_at, :updated_at
json.url patient_url(patient, format: :json)
