date_format = consultation.all_day_event? ? '%Y-%m-%d' _ '%Y-%m-%dT%H:%M:%S'

json.id consultation.id
json.patient_dni consultation.patient_dni
json.consult_code consultation.consult_code
json.start consultation.start.strftime(date_format)
json.end consultation.end.strftime(date_format)

json.allDay consultation.all_day_event? ? true : false

json.update_url consultation_path(consultation, method: :patch)
json.edit_url edit_consultation_path(consultation)