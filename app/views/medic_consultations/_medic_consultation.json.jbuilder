json.extract! medic_consultation, :id, :dni, :diagnosis, :medicine, :user_id, :created_at, :updated_at
json.url medic_consultation_url(medic_consultation, format: :json)
