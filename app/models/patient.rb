class Patient < ApplicationRecord
	has_many :has_medic_consultations
	has_many :medic_consultations, through: :has_medic_consultations
	#has_many :consultations
	validates :dni, :name, :last_name, :birthday, :age,:sex, presence: true
	validates :dni,:age, numericality: { only_integer: true }
	validates :age, length: { in: 1..120}
	validates :dni, length: { in: 8..10 }
	validates :name, :last_name, length: { in: 1..20}
	validates :sex, inclusion: {in: ["F","M"]}
end
