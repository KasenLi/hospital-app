class Diagnosis < ApplicationRecord
	validates :description, presence: true
	after_create :save_medicines

	has_many :diagnosis_medicines
	has_many :medicines, through: :diagnosis_medicines
	def medicines=(value)
		@medicines = value
	end

	def save_medicines
		@medicines.each do |medicine_id|
			DiagnosisMedicine.create(medicine_id: medicine_id,diagnosis_id: self.id)
		end
	end
end
