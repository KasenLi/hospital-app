class Consultation < ApplicationRecord
  belongs_to :user

  #validates :patient_dni, :consult_code, presence:true
  #validates :patient_dni, numericality: { only_integer: true }, length: { in:100000..100000000}
  #validates :consult_code, numericality: { only_integer: true }, length: { in:100..100000}

  attr_accessor :date_range

  def all_day_event?
    self.start == self.start.midnight && self.end == self.end.midnight ? true : false
  end
end
