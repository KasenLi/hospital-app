class DiagnosisMedicine < ApplicationRecord
  belongs_to :diagnosis
  belongs_to :medicine
  belongs_to :medic_consultation

  def self.all_for_consultation(diagnosis_id)
		DiagnosisMedicine.where(diagnosis_id: diagnosis_id)
	end

	def self.medicines_ids(diagnosis_id)
   medicines = DiagnosisMedicine.where(diagnosis_id: diagnosis_id)
   @medicines = []
   medicines.each do |medicine|
    puts medicine
    @medicines[@medicines.length] = medicine.medicine_id
   end
   return @medicines
  end
end
