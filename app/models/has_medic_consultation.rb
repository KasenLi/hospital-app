class HasMedicConsultation < ApplicationRecord
  belongs_to :patient
  belongs_to :medic_consultation
end
