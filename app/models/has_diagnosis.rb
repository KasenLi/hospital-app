class HasDiagnosis < ApplicationRecord
  belongs_to :medic_consultation
  belongs_to :diagnosis
end
