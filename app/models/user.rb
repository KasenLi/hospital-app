class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :medic_consultations
  has_many :consultations
  
  validates :name, :last_name, :birthday,:genre, presence: true
  validates :name, :last_name, length: {in: 1..20}
  validates :genre, inclusion: {in:  ["F","M"]}

end
