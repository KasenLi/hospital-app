class Medicine < ApplicationRecord
	validates :name, :description, :stock, presence: true
	validates :stock, numericality: { only_integer: true }
	validates :stock, length: { minimum: 0 }
	validates :name, length: { in: 1..100}
	def self.mostrar(medicine_id)
		Medicine.where(id: medicine_id)
	end
end
