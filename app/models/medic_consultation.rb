class MedicConsultation < ApplicationRecord
  belongs_to :user
  after_create :save_consultation
  has_many :has_diagnoses
  has_many :diagnoses, through: :has_diagnoses
  
  has_many :diagnosis_medicines
  has_many :medicines, through: :diagnosis_medicines

  validates :dni, presence: true, numericality: { only_integer: true }, length: { in:0..100000000}
  def diagnoses=(value)
		@diagnoses = value
	end

	def save_consultation
		@diagnoses.each do |diagnosis_id|
			HasDiagnosis.create(medic_consultation_id: self.id,diagnosis_id: diagnosis_id)
		end
	end
end
