class MainController < ApplicationController
  before_action :authenticate_user!
  def home
  end

  def set_layout
  	"application"
  end

end
