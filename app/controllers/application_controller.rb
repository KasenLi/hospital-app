class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  layout :set_layout
	before_action :configure_permitted_parameters, if: :devise_controller?
  def set_layout
  	"landing"
  end

  def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name,:last_name,:genre,:birthday])
    end
end
