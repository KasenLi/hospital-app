class MedicConsultationsController < ApplicationController
  before_action :set_medic_consultation, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  def set_layout
    "application"
  end
  # GET /medic_consultations
  # GET /medic_consultations.json
  def index
    @medic_consultations = MedicConsultation.all
    
  end
  def complete

  end
  # GET /medic_consultations/1
  # GET /medic_consultations/1.json
  def show
    @diagnosis_medicines = DiagnosisMedicine.all_for_consultation(@medic_consultation.diagnoses)
    @diagnosis_id = DiagnosisMedicine.medicines_ids(@medic_consultation.diagnoses) 
    @medicines = Medicine.mostrar(@diagnosis_id)
  end

  # GET /medic_consultations/new
  def new
    @diagnoses = Diagnosis.all
    @medic_consultation = MedicConsultation.new
    #@medicines = Medicine.where()
  end

  # GET /medic_consultations/1/edit
  def edit
    @diagnoses = Diagnosis.all
  end

  # POST /medic_consultations
  # POST /medic_consultations.json
  def create
    @consulta = false
    @dni = params[:dni]
    @patients = Patient.where(dni: @dni)
    if @patients.present?
      @medic_consultation = current_user.medic_consultations.new(medic_consultation_params)
      @medic_consultation.diagnoses = params[:diagnoses]
      respond_to do |format|
        
        if @medic_consultation.save
          format.html { redirect_to @medic_consultation, notice: 'Medic consultation was successfully created.' }
          format.json { render :show, status: :created, location: @medic_consultation }
        else
          format.html { render :new }
          format.json { render json: @medic_consultation.errors, status: :unprocessable_entity }
        end
      end
    else
      redirect_to new_patient_path, notice: 'Patient does not exist.'
      @consulta = true
    end
    

  end

  # PATCH/PUT /medic_consultations/1
  # PATCH/PUT /medic_consultations/1.json
  def update
    respond_to do |format|
      if @medic_consultation.update(medic_consultation_params)
        format.html { redirect_to @medic_consultation, notice: 'Medic consultation was successfully updated.' }
        format.json { render :show, status: :ok, location: @medic_consultation }
      else
        format.html { render :edit }
        format.json { render json: @medic_consultation.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /medic_consultations/1
  # DELETE /medic_consultations/1.json
  def destroy
    @medic_consultation.destroy
    respond_to do |format|
      format.html { redirect_to medic_consultations_url, notice: 'Medic consultation was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_medic_consultation
      @medic_consultation = MedicConsultation.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def medic_consultation_params
      params.permit(:dni, :diagnoses)
    end
end
