class WelcomeController < ApplicationController
  def index
  end

  def set_layout
  	return "landing" if action_name == "index"
  end
end
