# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180319143522) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "consultations", force: :cascade do |t|
    t.integer "patient_dni"
    t.integer "consult_code"
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "start"
    t.datetime "end"
    t.index ["user_id"], name: "index_consultations_on_user_id"
  end

  create_table "diagnoses", force: :cascade do |t|
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "diagnosis_medicines", force: :cascade do |t|
    t.bigint "diagnosis_id"
    t.bigint "medicine_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["diagnosis_id"], name: "index_diagnosis_medicines_on_diagnosis_id"
    t.index ["medicine_id"], name: "index_diagnosis_medicines_on_medicine_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "title"
    t.datetime "start"
    t.datetime "end"
    t.string "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "has_diagnoses", force: :cascade do |t|
    t.bigint "medic_consultation_id"
    t.bigint "diagnosis_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["diagnosis_id"], name: "index_has_diagnoses_on_diagnosis_id"
    t.index ["medic_consultation_id"], name: "index_has_diagnoses_on_medic_consultation_id"
  end

  create_table "has_medic_consultations", force: :cascade do |t|
    t.bigint "patient_id"
    t.bigint "medic_consultation_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["medic_consultation_id"], name: "index_has_medic_consultations_on_medic_consultation_id"
    t.index ["patient_id"], name: "index_has_medic_consultations_on_patient_id"
  end

  create_table "medic_consultations", force: :cascade do |t|
    t.bigint "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "dni"
    t.index ["user_id"], name: "index_medic_consultations_on_user_id"
  end

  create_table "medicines", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.integer "stock"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "dosis"
    t.text "observations"
  end

  create_table "patients", force: :cascade do |t|
    t.string "name"
    t.string "last_name"
    t.string "sex"
    t.date "birthday"
    t.integer "age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "dni"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "name"
    t.string "last_name"
    t.string "genre"
    t.date "birthday"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "consultations", "users"
  add_foreign_key "diagnosis_medicines", "diagnoses"
  add_foreign_key "diagnosis_medicines", "medicines"
  add_foreign_key "has_diagnoses", "diagnoses"
  add_foreign_key "has_diagnoses", "medic_consultations"
  add_foreign_key "has_medic_consultations", "medic_consultations"
  add_foreign_key "has_medic_consultations", "patients"
  add_foreign_key "medic_consultations", "users"
end
