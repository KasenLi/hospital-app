class AddDniToPatients < ActiveRecord::Migration[5.1]
  def change
    add_column :patients, :dni, :string
  end
end
