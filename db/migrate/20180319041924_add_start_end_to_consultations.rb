class AddStartEndToConsultations < ActiveRecord::Migration[5.1]
  def change
    add_column :consultations, :start, :datetime
    add_column :consultations, :end, :datetime
  end
end
