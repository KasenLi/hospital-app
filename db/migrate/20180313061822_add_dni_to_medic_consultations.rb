class AddDniToMedicConsultations < ActiveRecord::Migration[5.1]
  def change
    add_column :medic_consultations, :dni, :string
  end
end
