class CreateHasMedicConsultations < ActiveRecord::Migration[5.1]
  def change
    create_table :has_medic_consultations do |t|
      t.references :patient, foreign_key: true
      t.references :medic_consultation, foreign_key: true

      t.timestamps
    end
  end
end
