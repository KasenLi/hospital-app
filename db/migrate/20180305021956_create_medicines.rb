class CreateMedicines < ActiveRecord::Migration[5.1]
  def change
    create_table :medicines do |t|
      t.string :name
      t.text :description
      t.text :dosis
      t.text :observations
      t.integer :stock

      t.timestamps
    end
  end
end
