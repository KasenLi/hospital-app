class RemoveStartFromConsultations < ActiveRecord::Migration[5.1]
  def change
    remove_column :consultations, :start, :date
  end
end
