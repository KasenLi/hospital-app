class CreatePatients < ActiveRecord::Migration[5.1]
  def change
    create_table :patients do |t|
      t.string :dni
      t.string :name
      t.string :last_name
      t.string :sex
      t.date :birthday
      t.integer :age

      t.timestamps
    end
  end
end
