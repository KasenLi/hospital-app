class CreateConsultations < ActiveRecord::Migration[5.1]
  def change
    create_table :consultations do |t|
      t.integer :patient_dni
      t.integer :consult_code
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
