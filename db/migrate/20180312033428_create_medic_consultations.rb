class CreateMedicConsultations < ActiveRecord::Migration[5.1]
  def change
    create_table :medic_consultations do |t|
      t.string :dni
      t.string :diagnosis
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
