class RemoveDiagnosisFromMedicConsultations < ActiveRecord::Migration[5.1]
  def change
    remove_column :medic_consultations, :diagnosis, :string
  end
end
