class AddPatientDniToConsultations < ActiveRecord::Migration[5.1]
  def change
    add_column :consultations, :patient_dni, :integer
  end
end
