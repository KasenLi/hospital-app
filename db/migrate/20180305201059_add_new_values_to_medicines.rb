class AddNewValuesToMedicines < ActiveRecord::Migration[5.1]
  def change
    add_column :medicines, :dosis, :text
    add_column :medicines, :observations, :text
  end
end
