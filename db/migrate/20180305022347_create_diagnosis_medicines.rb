class CreateDiagnosisMedicines < ActiveRecord::Migration[5.1]
  def change
    create_table :diagnosis_medicines do |t|
      t.references :diagnosis, foreign_key: true
      t.references :medicine, foreign_key: true

      t.timestamps
    end
  end
end
