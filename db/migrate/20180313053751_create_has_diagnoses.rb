class CreateHasDiagnoses < ActiveRecord::Migration[5.1]
  def change
    create_table :has_diagnoses do |t|
      t.references :medic_consultation, foreign_key: true
      t.references :diagnosis, foreign_key: true

      t.timestamps
    end
  end
end
