class RemoveMedicineFromMedicConsultations < ActiveRecord::Migration[5.1]
  def change
    remove_column :medic_consultations, :medicine, :string
  end
end
