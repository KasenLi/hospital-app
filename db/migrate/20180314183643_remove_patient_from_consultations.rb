class RemovePatientFromConsultations < ActiveRecord::Migration[5.1]
  def change
    remove_column :consultations, :patient_id, :string
  end
end
