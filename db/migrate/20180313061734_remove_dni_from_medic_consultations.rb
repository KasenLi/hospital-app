class RemoveDniFromMedicConsultations < ActiveRecord::Migration[5.1]
  def change
    remove_column :medic_consultations, :dni, :integer
  end
end
